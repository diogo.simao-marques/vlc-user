.. VLC User Documentation master file, created by
   sphinx-quickstart on Mon Sep  9 19:49:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

################################
VLC |release| User Documentation
################################
 
Welcome to the User Documentation of `VLC media player <https://www.videolan.org/vlc/>`_, the free and open source application that **plays everything** and run on **all platforms**.

********
Sections
********

To get the most out of the VLC media player, start by reviewing a few introductory topics below;

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/index_desktop.jpg
         :target: ../desktop/index.html

      `Desktop <../desktop/index.html>`_
         Windows, macOS, Linux

   .. container:: descr

      .. figure:: /images/index_android.jpg
         :target: ../android/index.html

      `Android <../android/index.html>`_
         VLC for Android

   .. container:: descr

      .. figure:: /images/index_ios.jpg
         :target: ../ios/index.html

      `iOS <../ios/index.html>`_
         VLC for iOS

   .. container:: descr

      .. figure:: /images/index_faq.jpg
         :target: support/index.html

      :doc:`/support/index`
         General Help on VLC.

   .. container:: descr

      .. figure:: /images/index_lore.jpg
         :target: lore/index.html

      :doc:`/lore/index`
         One Upon a Time...

   .. container:: descr

      :ref:`Glossary<glossary>`
         Definitions for terms used in VLC and this documentation.


****
More
****

:doc:`lore/vlc/index`

:doc:`lore/videolan/index`




.. toctree::
   :maxdepth: 1
   :hidden:
   
   support/index.rst
   lore/index.rst
   glossary/index.rst
   about.rst
